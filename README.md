git clone https://gitlab.com/Yphanikumar5/jenkins-master.git

cd jenkins-master

docker-compose up -d

# Open the browser 

http://your-ip

username : admin

password : admin

=======================================================================================

# Ref docker deamon

https://nickjanetakis.com/blog/docker-tip-73-connecting-to-a-remote-docker-daemon

https://docs.docker.com/engine/security/https/

# Steps to run docker deamon with certificates

# Create the directory to store the configuration file using root user.


 mkdir -p /etc/systemd/system/docker.service.d

# Create a new file to store the daemon options.

vim  /etc/systemd/system/docker.service.d/options.conf

# Now make it look like this and save the file when you're done:

[Service]
ExecStart=
ExecStart=/usr/bin/dockerd --tlsverify --tlscacert=/root/ca.pem --tlscert=/root/server-cert.pem --tlskey=/root/server-key.pem \
  -H=0.0.0.0:2376
  

# Note give cerificates path correctly

Here /root is the directory where we generated the certificates

# Reload the systemd daemon.

systemctl daemon-reload

# Restart Docker.

systemctl restart docker

# Check the deamon is running or not

docker --tlsverify --tlscacert=/root/ca.pem --tlscert=/root/cert.pem --tlskey=/root/key.pem \
  -H=127.0.0.1:2376 version
  
# Certificates

Client Key : key.pem

Client certificate: cert.pem

Server CA certificate : server-cert.pem

=======================================================================================

# Ref jenkins master

https://github.com/Yphanikumar5/bitnami-docker-jenkins

https://hub.docker.com/r/phanikumary1995/jenkins-master

# Ref jenkins slave

https://github.com/Yphanikumar5/docker-ssh-slave

https://gitlab.com/Yphanikumar5/jenkins-slave

https://hub.docker.com/r/phanikumary1995/jenkins-slave
